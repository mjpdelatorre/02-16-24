import java.util.Calendar;
import java.util.Date;

public class TwoMonths_Activity {

    static Date twoMonthsLater(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Add two months
        cal.add(Calendar.MONTH, 2);

        return cal.getTime();
    }

    public static void main(String[] args){
        Calendar cal = Calendar.getInstance();
        cal.set(2014, Calendar.DECEMBER, 31);
        System.out.println(twoMonthsLater(cal.getTime()));
    }
}
