import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import static java.time.Month.DECEMBER;

public class Localdt_Activity {
    public static void main(String[] args){
        ZonedDateTime harareDeparture = ZonedDateTime.of(java.time.LocalDateTime.of(2015, DECEMBER, 1, 9, 15), ZoneId.of("Africa/Harare"));

        Duration harareFlightTime = Duration.ofHours(20);
        java.time.LocalDateTime arrival = arrival(harareDeparture, harareFlightTime);
        System.out.println(arrival);
    }

    static java.time.LocalDateTime arrival(ZonedDateTime departure, Duration flightTime){
        ZoneId here = ZoneId.systemDefault();
        ZonedDateTime newZonedDateTime = departure.withZoneSameInstant(here);
        return newZonedDateTime.plus(flightTime).toLocalDateTime();
    }
}
